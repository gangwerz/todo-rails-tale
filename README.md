# _Todo_: A Ruby-on-Rails Story

## Background

I am presently looking for a development position, and I have seen that the Rails ecosystem is alive and well. So, I decided to spin up the classic Todo App and stretch my Rails skills.

## My Rails History

I first used Rails in 2013, at the height of Rails' influence. At that point, I had very little web development experience. I had used WYSIWYG builders, than moved into HTML, later JQuery, and then I had a class with some PHP. Rails at that point was very different from what I was used to, at a conceptual level. My coursework never used any frameworks, so learning about MVC design and generators was all new to me. After getting comfortable, I ran through a few tutorials and continued play around with the framework until roughly 2016. At that point, MEAN was the hot new stack, and I started diving into the JS world. While I never made anything flashy in Rails, I like how rapidly I could get to a respectably complete product, and concepts like routing and templating made more sense to me in Ruby than PHP.

## Development Roadmap

- [x] Tutorial

- [ ] Style Overhaul

- [ ] Form Validation

- [ ] User Accounts
